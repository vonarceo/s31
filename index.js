const http = require("http")

const port = 3000

const server = http.createServer((request, response) => {
	if(request.url == '/login') {
		response.writeHead(200, {'Context-type': 'text/plain'})
		response.end('welcome to the login page.')
	
	} else {
		response.writeHead(404, {'Context-type': 'text/plain'})
		response.end('im sorry the page you are looking for cannot be found.')
	}
})

server.listen(port);

console.log('server is successfully running');

