// 1. What directive is used by Node.js in loading the modules it needs?

Answer: node (jsname).js

// 2. What Node.js module contains a method for server creation?

Answer: http.createServer

// 3. What is the method of the http object responsible for creating a server using Node.js?

Answer: declaring a variable that has a require("http") for the client and server to communicate / interact.
		and "http.createServer((request, response))" that accept 2 arguement in a function. 

// 4. What method of the response object allows us to set status codes and content types?
	
Answer: in response.writeHead

// 5. Where will console.log() output its contents when run in Node.js?

Answer: in gitbash terminal

// 6. What property of the request object contains the address's endpoint?

Answer: in request.url